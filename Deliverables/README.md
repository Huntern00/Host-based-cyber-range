README.txt

This folder contains the virtual machine OVA files to be imported to your hypervisor.  The kali.ova contains the scenarios for telnet, reverse shell, and email spoofing.  The other OVA contains the scenarios for XSS, SQL Injection, and Command Injection.  The reamining information you will need can be found in the Docs folder.

Due to the size of the OVA files we had to upload them to a dropbox which is linked below. 

https://www.dropbox.com/s/vksknlf5uzy4t15/Deliverables.rar?dl=0
