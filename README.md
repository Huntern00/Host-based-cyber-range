README.md

Team Info:
Derek Wyld;  dw16@hood.edu;  (BS, 22)
Hunter Nelson;  hn5@hood.edu;  (BS, 22)

Project Info:
We built a host-based cyber range to provide easier access to environments and tools to provide training to cyber security professionals.  This method is to help change the trend of having a shortage of cybersecurity professionals throughout society.

Project Repository: https://gitlab.com/Huntern00/Host-based-cyber-range

The repository is split into clearly labeled folders.  The OVA files to be downloaded can be found in the main branch main folder, while all of the tutorials and walkthroughs can be found in the documentation folder.

Tech Stack:
VM's, Linux, WSL, Bash

All configuration details can be found in the documentation of each scenario under the documentation folder in the project respository.
